<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about an archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            $this->db = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4'
            , DB_USER, DB_PWD, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
        $eventList = array();

        // DONE: Retrive events from the database and add to the list, one by one
        $stmt = $this->db->query('SELECT id, title, date, description FROM event ORDER BY date');

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          $eventList[] = new Event($row['title'], $row['date'], $row['description'], $row['id']);
        }

        return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        $event = null;

        // DONE: Retrive the event from the database
        $row = $this->db->query('SELECT * FROM event WHERE id='. $id)->fetch(PDO::FETCH_ASSOC);
        
        $event = new Event($row['title'], $row['date'], $row['description'], $row['id']);
        $event->verify();
        
        return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        // DONE: Add the event to the database

      $event->verify(true);
      $stmt = $this->db->prepare('INSERT INTO event (title, date, description) VALUES (?, ?, ?)');

      $stmt->bindValue(1, $event->title);
      $stmt->bindValue(2, $event->date);
      $stmt->bindValue(3, $event->description);

      $stmt->execute();

      $event->id = $this->db->lastInsertId();

    }

    /** Modifies data related to an event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
        // DONE: Modify the event in the database
        
        $event->verify(true);
        $stmt = $this->db->prepare('UPDATE event SET title = :title, date = :date, description = :description WHERE event.id = :id');

        $stmt->bindValue(':title', $event->title);
        $stmt->bindValue(':date', $event->date);
        $stmt->bindValue(':description', $event->description);
        $stmt->bindValue(':id', $event->id);

        $stmt->execute();
        
        
        
    }

    /** Deletes data related to an event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        // DONESO: Delete the event from the database
        $stmt = $this->db->prepare('DELETE FROM event WHERE id = ' . $id);
        $stmt->execute();
    }
}
